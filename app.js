const http = require("http");
const url = require("url");
const SpotifyWebApi = require("spotify-web-api-node");
const { WebClient } = require("@slack/web-api");
const axios = require("axios").default;
require("dotenv").config();

// Spotify Credentials
const spotifyClientId = process.env.SPOTIFY_CLIENT_ID;
const spotifyClientSecret = process.env.SPOTIFY_CLIENT_SECRET;
const spotifyRedirectUri = process.env.SPOTIFY_REDIRECT_URI;
const spotifyScopes = ["user-read-currently-playing"];

// Slack Credentials
const slackBotToken = process.env.SLACK_BOT_TOKEN;

let statusText = "Log in to Spotify to retrieve your currently playing song.";

// Create the Spotify API client
const spotifyApi = new SpotifyWebApi({
  clientId: spotifyClientId,
  clientSecret: spotifyClientSecret,
  redirectUri: spotifyRedirectUri,
});

// Create the Slack API client
const slackClient = new WebClient(slackBotToken);

// Function to update the Slack status
function updateSlackStatus(statusText) {
  console.log("Updating Slack status to:", statusText);

  slackClient.users.profile
    .set({
      profile: {
        status_text: statusText,
        status_emoji: ":musical_note:",
      },
    })
    .then((res) => {
      console.log("Successfully updated Slack status:", res);
    })
    .catch((err) => {
      console.log("Failed to update Slack status:", err);
    });
}

// Function to get the current Spotify song
// Function to get the current Spotify song
function getCurrentSpotifySong() {
  // Check if access token is set
  if (spotifyApi.getAccessToken()) {
    return spotifyApi.getMyCurrentPlayingTrack().then(
      function (data) {
        if (!data.body.item) {
          // No song is currently playing
          statusText = "Not currently listening to anything on Spotify.";
        } else {
          // Extract song information from API response
          const song = data.body.item.name;
          const artist = data.body.item.artists[0].name;
          const album = data.body.item.album.name;
          statusText = `Listening to ${song} by ${artist}`;
          console.log(
            "Successfully retrieved current Spotify song:",
            song,
            artist,
            album
          );
          updateSlackStatus(statusText);
        }
        return statusText;
      },
      function (err) {
        console.log("Failed to get current Spotify song:", err);
        return "";
      }
    );
  } else {
    return Promise.resolve("");
  }
}

// Function to handle errors
function handleError(res, error) {
  console.error(error);
  res.writeHead(500, { "Content-Type": "text/plain" });
  res.write("Error: " + error.message);
  res.end();
}

// Create the HTTP server
const server = http.createServer(function (req, res) {
  const requestUrl = url.parse(req.url, true);

  if (requestUrl.pathname === "/") {
    res.writeHead(200, { "Content-Type": "text/html" });

    // Check if login was successful
    const loginSuccess = requestUrl.query.success;
    if (loginSuccess === "true") {
      res.write(`
    <html>
      <body>
        <h1>Login successful!</h1>
        <p id="current-song">Loading current song...</p>
        <script>
          const currentSongElement = document.getElementById('current-song');

          function refreshCurrentSong() {
            fetch('/current-song')
              .then(response => response.json())
              .then(data => {
                currentSongElement.textContent = 'Current song: ' + data.currentSong;
              });
          }

          // Refresh the song every 5 seconds
          setInterval(refreshCurrentSong, 5000);

          // Get the current song as soon as the login is successful
          fetch('/current-song')
            .then(response => response.json())
            .then(data => {
              currentSongElement.textContent = 'Current song: ' + data.currentSong;
            });
        </script>
      </body>
    </html>
  `);
    } else {
      // Display login form
      res.write(`
        <html>
          <body>
            <h1>Update Slack Status</h1>
            <form id="login-form" action="/login" method="get">
              <button id="login-button" type="submit">Log in to Spotify</button>
            </form>
            <script>
              const loginForm = document.getElementById('login-form');
              const loginButton = document.getElementById('login-button');
            </script>
          </body>
        </html>
      `);
    }
    res.end();
  } else if (requestUrl.pathname === "/current-song") {
    getCurrentSpotifySong()
      .then((currentSong) => {
        res.writeHead(200, { "Content-Type": "application/json" });
        res.write(JSON.stringify({ currentSong }));
        console.log(currentSong);
        res.end();
      })
      .catch((err) => {
        handleError(res, err);
      });
  } else if (requestUrl.pathname === "/login") {
    const state = Math.random().toString(36).substring(7);
    const authorizeUrl = spotifyApi.createAuthorizeURL(spotifyScopes, state);
    res.writeHead(302, { Location: authorizeUrl });
    res.end();
  } else if (requestUrl.pathname === "/callback") {
    const { code, state } = requestUrl.query;

    // Retrieve an access token and a refresh token
    spotifyApi.authorizationCodeGrant(code).then(
      function (data) {
        console.log("Successfully authorized with Spotify!");
        const accessToken = data.body.access_token;
        const refreshToken = data.body.refresh_token;
        const expiresIn = data.body.expires_in;

        // Set the access token and refresh token
        spotifyApi.setAccessToken(accessToken);
        spotifyApi.setRefreshToken(refreshToken);

        // Schedule a function to update the Slack status when the access token expires
        const expirationTime = Date.now() + expiresIn * 1000;
        setTimeout(function () {
          getCurrentSpotifySong().then((statusText) => {
            
            axios
              .post(
                `https://api.spotify.com/v1/me/player/play`,
                {
                  uris: [],
                  offset: {
                    position: 0,
                  },
                },
                {
                  headers: {
                    Authorization: "Bearer " + spotifyApi.getAccessToken(),
                    "Content-Type": "application/json",
                  },
                }
              )
              .then(() => console.log("Skipped to next track"))
              .catch((err) =>
                console.log("Failed to skip to next track:", err)
              );
          });
        }, expirationTime - Date.now());

        // Redirect the user to the homepage with success message
        res.writeHead(302, { Location: "/?success=true" });
        res.end();
      },
      function (error) {
        handleError(res, error);
      }
    );
  }
});

const port = process.env.PORT || 3000;
server.listen(port, function () {
  console.log("Server listening on port", port);
});
